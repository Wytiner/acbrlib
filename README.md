# AcbrLibTeste

Este projeto é um exemplo de como usar a ACBrLib para realizar operações SAT em uma aplicação Windows Forms. Ele também demonstra como estruturar o código para ser testável e modular.

## Configuração

Para configurar o projeto, siga estas etapas:

1. Clone o repositório.
2. Abra o projeto no Visual Studio.
3. Configure a string de conexão do banco de dados no arquivo `App.config`.
4. Execute o projeto.

## Uso

Para usar o projeto, siga estas etapas:

1. Inicie a aplicação.
2. Selecione um produto na lista.
3. Clique no botão 'Venda' para realizar uma venda.

## Contribuindo

Se você quiser contribuir para o projeto, siga estas etapas:

1. Fork o repositório.
2. Faça suas alterações.
3. Faça um pull request.

## Licença

Este projeto está licenciado sob a MIT License. Veja o arquivo `LICENSE` para mais detalhes.
