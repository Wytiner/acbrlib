using System;
using System.Windows.Forms;
using AcbrLibTeste.Data;
using AcbrLibTeste.Data.Repositories;
using AcbrLibTeste.Services;

namespace AcbrLibTeste
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Set up dependencies
            var context = new Context();
            var productRepository = new ProductRepository(context);
            var productService = new ProductService(productRepository);

            // Create and show the main form
            var mainForm = new MainForm(productService);
            Application.Run(mainForm);
        }
    }
}
