using System.Collections.Generic;
using System.Threading.Tasks;
using AcbrLibTeste.Data.Entities;
using AcbrLibTeste.Data.Interfaces;

namespace AcbrLibTeste.Services
{
    public class ProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<IEnumerable<Product>> GetAllProductsAsync()
        {
            return await _productRepository.GetAllAsync();
        }

        public async Task<Product> GetProductByIdAsync(int id)
        {
            return await _productRepository.GetByIdAsync(id);
        }

        public async Task AddProductAsync(Product product)
        {
            await _productRepository.AddAsync(product);
        }

        public async Task UpdateProductAsync(Product product)
        {
            await _productRepository.UpdateAsync(product);
        }

        public async Task DeleteProductAsync(Product product)
        {
            await _productRepository.DeleteAsync(product);
        }

        public async Task<int> GetTotalProductsAsync()
        {
            using (var context = new Context())
            {
                return await context.Products.CountAsync();
            }
        }

    }
}
