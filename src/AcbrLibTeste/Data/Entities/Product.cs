using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcbrLibTeste.Data.Entities
{
    [Table("products")]
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public bool Active { get; set; } = true;

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(30)]
        public string InternalCode { get; set; }

        [MaxLength(13)]
        public string Gtin { get; set; }

        public decimal UnitaryValue { get; set; } = 0.00m;

        public int Score { get; set; } = 0;

        public bool AllowInvoice { get; set; } = false;

        public bool InformValue { get; set; } = false;

        public bool InformName { get; set; } = false;

        public decimal WholesaleValue { get; set; } = 0.00m;

        public decimal WholesaleQuantity { get; set; } = 0.000m;

        public bool FractionalSale { get; set; } = false;

        public bool IsCommissioned { get; set; } = false;

        public decimal CommissionAmount { get; set; } = 0.00m;

        public decimal CommissionPercentage { get; set; } = 0.000m;

        public int? CFOP { get; set; }

        [MaxLength(15)]
        public string NCM { get; set; }

        public int ICMS_Origem { get; set; } = 0;

        public int? ICMS_Cst { get; set; }

        public decimal ICMS_Aliq { get; set; } = 0.000m;

        public int? PIS_Cst { get; set; }

        public decimal PIS_Aliq { get; set; } = 0.000m;

        public decimal PIS_PerReducao { get; set; } = 0.000m;

        public decimal PIS_StAliqProd { get; set; } = 0.000m;

        public int? COFINS_Cst { get; set; }

        public decimal COFINS_Aliq { get; set; } = 0.000m;

        public decimal COFINS_PerReducao { get; set; } = 0.000m;

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
