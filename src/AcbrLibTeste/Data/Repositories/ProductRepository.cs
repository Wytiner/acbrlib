using System.Data.Entity;
using System.Threading.Tasks;
using AcbrLibTeste.Data.Entities;
using AcbrLibTeste.Data.Interfaces;

namespace AcbrLibTeste.Data.Repositories
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(Context context) : base(context) { }
        
        // Implement additional methods specific to the Product table if necessary
    }
}
