using System.Collections.Generic;
using System.Threading.Tasks;
using AcbrLibTeste.Data.Entities;

namespace AcbrLibTeste.Data.Interfaces
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetAllAsync();
        Task<Product> GetByIdAsync(int id);
        Task AddAsync(Product product);
        Task UpdateAsync(Product product);
        Task DeleteAsync(Product product);
    }
}
