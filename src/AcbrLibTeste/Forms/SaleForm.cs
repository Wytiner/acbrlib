using System;
using System.Windows.Forms;
using AcbrLibTeste.Services;
using AcbrLibTeste.ACBrLib;

namespace AcbrLibTeste.Forms
{
    public partial class SaleForm : Form
    {
        private readonly ProductService _productService;
        private readonly ACBrLibIntegracao _acbrLibIntegracao;

        public SaleForm(ProductService productService, ACBrLibIntegracao acbrLibIntegracao)
        {
            InitializeComponent();
            _productService = productService;
            _acbrLibIntegracao = acbrLibIntegracao;
        }

        private async void SaleForm_Load(object sender, EventArgs e)
        {
            var products = await _productService.GetAllProductsAsync();
            // Adicione o código para exibir os produtos na interface do usuário
        }

        private void ButtonSale_Click(object sender, EventArgs e)
        {
            // Quando o botão de venda é clicado, realize a venda usando ACBrLib
            _acbrLibIntegracao.EmitirCupomFiscal();
        }
    }
}
