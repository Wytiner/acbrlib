using System;
using System.Windows.Forms;
using AcbrLibTeste.Services;

namespace AcbrLibTeste.Forms
{
    public partial class MainForm : Form
    {
        private const int PageSize = 10; // The number of items per page
        private int currentPage = 1; // The current page number
        private ProductService productService;

        public MainForm(ProductService productService)
        {
            InitializeComponent();
            this.productService = productService;
            LoadData();
        }

        private async void LoadData()
        {
            // Get the products for the current page
            var products = await productService.GetProductsAsync(currentPage, PageSize);

            // Clear the old rows
            dataGridView1.Rows.Clear();

            // Add the new rows
            foreach (var product in products)
            {
                dataGridView1.Rows.Add(product.Id, product.Name ?? "", product.Price);
            }

            // Update the paginator controls
            UpdatePaginator();
        }

        private async void UpdatePaginator()
        {
            // Get the total number of products
            int totalProducts = await productService.GetTotalProductsAsync();

            // Calculate the total number of pages
            int totalPages = (totalProducts + PageSize - 1) / PageSize;

            // Enable or disable the paginator buttons based on the current page number
            btnPreviousPage.Enabled = currentPage > 1;
            btnNextPage.Enabled = currentPage < totalPages;

            // Update the page number label
            lblPageNumber.Text = $"{currentPage} / {totalPages}";
        }


        private void btnPreviousPage_Click(object sender, EventArgs e)
        {
            // Go to the previous page
            currentPage--;
            LoadData();
        }

        private void btnNextPage_Click(object sender, EventArgs e)
        {
            // Go to the next page
            currentPage++;
            LoadData();
        }

    }
}
