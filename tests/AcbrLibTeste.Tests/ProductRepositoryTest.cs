using System.Data.Entity;
using AcbrLibTeste.Data.Entities;

namespace AcbrLibTeste.Data
{
    public class Context : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public Context() : base("Server=localhost;Database=sfpdvcaixa;Uid=root;Pwd=;")
        {
            // Database.SetInitializer(new CreateDatabaseIfNotExists<Context>());
            // Database.SetInitializer(new DropCreateDatabaseIfModelChanges<Context>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Fluent API configurations here

            base.OnModelCreating(modelBuilder);
        }
    }
}
