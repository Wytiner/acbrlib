using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AcbrLibTeste;

namespace AcbrLibTeste.Tests
{
    [TestClass]
    public class ProgramTest
    {
        [TestMethod]
        public void Main_RunsWithoutException()
        {
            try
            {
                Program.Main();
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail("Exception was thrown");
            }
        }
    }
}
